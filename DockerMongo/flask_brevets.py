"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#db = client.brevdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    # values from json 
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=int)
    start_date = request.args.get('begin_date', type=str)
    start_time = request.args.get('begin_time', type=str)
    combined_time = start_date + " " + start_time

    # if either of the date/time are not filled in then how am i to find the end date?
    if start_time == "" or start_date == "":
        open_time = None
        close_time = None
    
    else:
        combined_time_arrow = arrow.get(combined_time, 'YYYY-MM-DD HH:mm')
        print(combined_time_arrow)
        app.logger.debug("km={}".format(km))
        app.logger.debug("request.args: {}".format(request.args))
        # FIXME: These probably aren't the right open and close times
        # and brevets may be longer than 200km
        open_time = acp_times.open_time(km, distance, combined_time_arrow)
        close_time = acp_times.close_time(km, distance, combined_time_arrow)
    result = {"open": open_time.isoformat(), "close": close_time.isoformat()}
    return flask.jsonify(result=result)



# proj 5 starts here kinda

@app.route('/submit')
def submit():
    data = requst.args.get('res')
    open_time = request.args.get('open_time')
    close_time = request.args.get('close_time')
    distance = request.args.get('distance')
    dist_dict = {}
    dist_dict['distance'] = distance

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
