import acp_times
import arrow

test_start = arrow.get("2017-01-01 00:00", "YYYY-MM-DD HH:mm")

def test_open_60():
    output = acp_times.open_time(60, 200, test_start)
    expected_output = test_start.clone().shift(hours=1, minutes=46)
    assert output == expected_output

def test_close_60():
    output = acp_times.close_time(60, 200, test_start)
    expected_output = test_start.clone().shift(hours=4, minutes=0)
    assert output == expected_output

def test_open_550():
    output = acp_times.open_time(550, 700, test_start)
    expected_output = test_start.clone().shift(hours=17, minutes=8)
    assert output == expected_output

def test_close_550():
    output = acp_times.close_time(550, 600, test_start)
    expected_output = test_start.clone().shift(hours=36, minutes=40)
    assert output == expected_output

def test_open_890():
    output = acp_times.open_time(890, 1000, test_start)
    expected_output = test_start.clone().shift(hours=29, minutes=9)
    assert output == expected_output

def test_close_890():
    output = acp_times.close_time(890, 1000, test_start)
    expected_output = test_start.clone().shift(hours=65, minutes=23)
    assert output == expected_output
    
